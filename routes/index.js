const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const middleware = jwt.authenticateToken;
const usersController = require("../controller/usersController");
const testController = require("../controller/testController");

module.exports = app => {
  // router.get("/generate", jwt.generateTokenSecret);
  router.get("/users", usersController.listUsers);
  router.post("/register", usersController.register);
  router.post("/login", usersController.login);

  router.get("/admin", middleware("admin"), testController.admin);
  router.get("/customer", middleware("customer"), testController.customer);

  app.use("/api", router);
};

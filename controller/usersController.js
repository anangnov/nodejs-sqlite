"use-strict";

const md5 = require("md5");
const Validator = require("validatorjs");
const models = require("../models");
const jwt = require("../utils/jwt");

const listUsers = async (req, res) => {
  try {
    const getData = await models.users.findAll({});

    let objectResponse = await {
      error: false,
      message: "Success",
      data: getData
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const register = async (req, res) => {
  try {
    const rules = {
      fullname: "required",
      phone: "required",
      email: "required|email",
      password: "required|min:6",
      role_id: "required|between:1,2"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        200
      );
    }

    req.body.password = md5(req.body.password);

    await models.users.create(req.body);
    const role = req.body.role_id === 1 ? "Administrator" : "Customer";

    let objectResponse = await {
      error: false,
      message: "Success",
      info: `Success register as ${role}`
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const login = async (req, res) => {
  try {
    const rules = {
      email: "required",
      password: "required"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        200
      );
    }

    req.body.password = md5(req.body.password);

    const checkUsers = await models.users.findOne({
      where: {
        email: req.body.email,
        password: req.body.password
      }
    });

    if (!checkUsers) {
      return req.output(
        req,
        res,
        { message: "Email or Password not found" },
        "info",
        200
      );
    }

    const token = jwt.generateAccessToken(checkUsers.dataValues);

    let objectResponse = await {
      error: false,
      message: "Success",
      data: {
        token: token
      }
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

module.exports = { listUsers, register, login };

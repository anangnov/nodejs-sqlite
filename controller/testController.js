"use-strict";

const models = require("../models");

const admin = async (req, res) => {
  try {
    let objectResponse = await {
      error: false,
      message: "Success",
      data: {
        info: "Hello World Admin"
      }
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const customer = async (req, res) => {
  try {
    let objectResponse = await {
      error: false,
      message: "Success",
      data: {
        info: "Hello World Customer"
      }
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

module.exports = { admin, customer };

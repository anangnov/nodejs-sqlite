const jwt = require("jsonwebtoken");
const models = require("../models");

function authenticateToken(role) {
  return function(req, res, next) {
    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];
    if (token == null)
      return req.output(
        req,
        res,
        {
          error: true,
          message: "Unauthorized"
        },
        "error",
        401
      );

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      if (user) {
        const roleName = user.data.role_id === 1 ? "admin" : "customer";

        if (role !== roleName) {
          return req.output(
            req,
            res,
            {
              error: true,
              message: "Unauthentication not " + role
            },
            "error",
            403
          );
        }
      }

      if (err)
        return req.output(
          req,
          res,
          {
            error: true,
            message: "Unauthentication"
          },
          "error",
          403
        );

      // console.log(user);
      next();
    });
  };
}

const checkRole = async user => {
  const getUsers = await models.users.findOne({
    where: {
      email: user.data.email,
      role_id: user.data.role_id,
      password: user.data.password
    }
  });

  return getUsers;
};

function generateAccessToken(payload) {
  return jwt.sign({ data: payload }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: process.env.EXPIRED_TOKEN
  });
}

function generateTokenSecret(req, res) {
  let token = require("crypto")
    .randomBytes(32)
    .toString("hex");

  return res.json({
    message: token
  });
}

module.exports = {
  authenticateToken,
  generateAccessToken,
  generateTokenSecret
};

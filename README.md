# node-test

### Features
- Controller
- Routes
- Model
- Sequelize (ORM)

### Quick Start

Install package modules
        
    npm install

Run application
    
    npm start

Documentation Swagger
    
    http://localhost:3002/api-docs

Testing user
    
    administrator: 
    
    email: admin@gmail.com
    password: 123456

    email: customer@gmail.com
    password: 123456


## License

[MIT](LICENSE)

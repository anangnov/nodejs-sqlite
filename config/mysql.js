"use strict";

module.exports = {
  storage: process.env.STORAGE_NAME,
  dialect: "sqlite",
  logging: true
};
